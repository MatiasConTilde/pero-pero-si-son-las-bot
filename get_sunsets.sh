#!/bin/bash

timechange=1 # 1 for winter, 2 for summer

for j in {11..12}; do
  for i in {01..31}; do
    time=$(curl -s "https://api.sunrise-sunset.org/json?lat=40.4086&lng=-3.6922&date=2023-$j-$i&formatted=0" | jq -r '.results.civil_twilight_end')
    date=$(echo "$time" | cut -c -10 | sed 's/-/_/g')
    hour_n=$(echo "$time" | cut -c 12-13)
    hour_r=$(echo "$time" | cut -c 14-16)
    hour=$((hour_n + timechange))$hour_r
    echo "$time $date $hour"
    convert -background transparent -fill white -stroke black -strokewidth 5 -font Anton-Regular -pointsize 120 label:"$hour" -resize x65% hour.png
    echo convert base.jpg hour.png -geometry +820+650 -composite "img/${date}_${hour//:/_}.jpg"
  done
done
rm img/_.jpg img/_1.jpg img/_2jpg
