import asyncio
from telethon import TelegramClient
from os import listdir
from datetime import datetime, timedelta
from telethon.errors.rpcerrorlist import ScheduleTooMuchError

SESSION_NAME = 'peroperosisonlas'
API_ID = 12345678
API_HASH = '0123456789abcdef0123456789abcdef'

timechange_start = datetime(2023, 3, 26, 2, 0)
timechange_end = datetime(2023, 10, 29, 3, 0)


async def main():
    async with TelegramClient(SESSION_NAME, API_ID, API_HASH) as client:
        img_list = sorted(listdir('img'))

        # await client.send_message('@pero_perosisonlas_todos_los_dias', 'cambio de hora gente! ahora son las 3', schedule=timechange_start-timedelta(hours=1))
        # await client.send_message('@pero_perosisonlas_todos_los_dias', 'cambio de hora gente! ahora son las 2', schedule=timechange_end-timedelta(hours=2))
        # return

        for f in img_list:
            year, month, day, hour, minute = [int(x) for x in f[:-4].split('_')]
            when = datetime(year, month, day, hour-1, minute)

            if timechange_start < when < timechange_end:
                when = when - timedelta(hours=1)

            try:
                if hour == 18 and minute == 30:
                    await client.send_file('@pero_perosisonlas_todos_los_dias', 'original.jpg', schedule=when, caption='meme original!')
                else:
                    await client.send_file('@pero_perosisonlas_todos_los_dias', 'img/'+f, schedule=when)
            except ScheduleTooMuchError:
                await client.send_message('@matib0t', 'ultimo mensaje semana que viene', schedule=when-timedelta(weeks=1))
                await client.send_message('@matib0t', 'ultimo mensaje mañana', schedule=when-timedelta(days=2))
                break

asyncio.run(main())
